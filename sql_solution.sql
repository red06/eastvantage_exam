with main_table AS (
	select * from orders o 
	left join sales s on s.sales_id = o.sales_id 
	left join customers c on c.customer_id  = s.customer_id
	left join items i on i.item_id = o.item_id
	where c.age BETWEEN 18 and 35
),
items_per_customer as (
	select customer_id, age,item_name, sum(quantity) as quantity
	from main_table group by customer_id, item_name
)
select count(1) from items_per_customer where quantity is not null order by customer_id, item_name, quantity
import pandas as pd

cust = pd.read_csv(
    "customers.csv", dtype={"customer_id": "Int64", "age": "Int64"}
).set_index("customer_id")
orders = pd.read_csv(
    "orders.csv",
    dtype={
        "order_id": "Int64",
        "sales_id": "Int64",
        "item_id": "Int64",
        "quantity": "Int64",
    },
)
items = pd.read_csv(
    "items.csv", dtype={"items_id": "Int64", "item_name": "str"}
).set_index("item_id")
sales = pd.read_csv(
    "sales.csv", dtype={"customer_id": "Int64", "sales_id": "Int64"}
).set_index("sales_id")

orders_sales = orders.join(sales, on="sales_id", rsuffix="_tblsales", how="left")
merge_cust = orders_sales.join(cust, on="customer_id", rsuffix="_tblcust", how="left")
filtered_by_age = merge_cust[merge_cust["age"].between(18, 35)]
merge_items = filtered_by_age.join(items, on="item_id", rsuffix="_tblitem", how="left")

grouped_df = merge_items.groupby(
    ["customer_id", "age", "item_name"], as_index=False
).agg({"quantity": "sum"})
remove_null = grouped_df[grouped_df["quantity"] > 0]
remove_null.sort_values(by="customer_id").to_csv("pure_pandas.csv", index=False, sep=";")
